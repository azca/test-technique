import React, { Component } from "react";
import './autocomplete.css';

class Autocomplete extends React.Component {
  constructor() {
    super();
    this.state = {
      list: [],
      isEmpty: true,
      text: '',
      open: false,
      selectValue: null
    }
    this.onType = this.onType.bind(this);
    this.clearText = this.clearText.bind(this);
    this.openSearch = this.openSearch.bind(this);
    this.closeSearch = this.closeSearch.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  
  componentWillMount() {
    this.setState({ list: this.props.data.slice() });
  }
  
  onType(str, wasClicked) {
    let isEmpty = !str.length;
    let searchList = this.props.data;

    

    if (wasClicked) {
      searchList = searchList.filter(data => data.name.includes(str));

    } else {
      str = str.toLowerCase();
      searchList = searchList.filter(data => data.name.toLowerCase().includes(str));
    }    
    
    this.setState({
      list: searchList,
      isEmpty: isEmpty,
      text: str
    });
  }

  onChange = (data) => {
    this.setState({
      selectValue: data
    });
    
  }
  
  openSearch() {
    this.setState({ open: true });
  }

  closeSearch() {
    this.setState({ open: false });
  }
  
  clearText() {
    this.setState({
      list: this.props.data.slice(),
      isEmpty: true,
      text: '',
      selectValue: null
    });    
  }
  
  render() {
    let {list, isEmpty, text} = this.state;
    
    let cancel = !isEmpty ? 
        <span className="cancel" 
          onClick={() => {
            this.clearText();
            setTimeout(() => this.openSearch(), 25);
          }}>Cancel</span> : null;
  
    
    return (
      <div className="content" onClick={this.closeSearch}>
        <div className="autocomplete">
          <input type="text" placeholder="recherche" 
            value={text}
            onClick={(e) => {
              this.openSearch();
              e.stopPropagation();
            }} 
            onChange={(e) => this.onType(e.target.value)}/>
          {cancel}
          <div className={this.state.open === true ? 'search open' : 'search'} onClick={(e) => this.onType(e.target.textContent, true)}>
            {list.map(data => {
              return (
                data.hexa ? (
                  <div key={data.id} onClick={(e) => this.onChange(data)}><span style={{ "margin-right": "2em", padding: "1em", background: data.hexa }}/>{data.name}</div>
                ) : (
                  <div key={data.id} onClick={(e) => this.onChange(data)}>{data.name}</div>
                )
              )
            })}
          </div>
        </div>

        <br/>
        <br/>
        <br/>

        <div><h2>Vous avez sélectionner : {this.state.selectValue ? this.state.selectValue.name : 'rien'}</h2></div>
      </div>
    );
  }
}

export default Autocomplete;

