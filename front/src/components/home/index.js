import React, { Component } from "react";
import { Container, Row, Col } from 'reactstrap';
import './home.css';
import gql from 'graphql-tag';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import Autocomplete from '../autocomplete'
import { setContext } from 'apollo-link-context';

// const client = new ApolloClient({
//   link: createHttpLink({ uri: 'http://localhost:4000/graphql' }),
//   cache: new InMemoryCache(),
// });

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/graphql',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = "my-awsome-secure-token";
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token,
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataResult: false
    };
  }
  
  componentWillMount = () => {
    client
      .query({
        query: gql`
          {
            products {
              idProduct
              name
              color {
                name
              }
            }
            colors {
              idColor
              name
              hexa
            }
          }
        `
      })
      .then(result => {
        const data = result.data.products.concat(result.data.colors)
        this.setState({
          data,
          dataResult: true
        });
      });
  }

  render() {
    return (
      this.state.dataResult === true ? (
        <div>
          <Container>
            <Row>
              <Col><h1>Home</h1></Col>
            </Row>
            <Autocomplete data={this.state.data}/>
          </Container>
        </div>
      ) : (
        <div>
          <Container>
            <Row>
              <Col><h1>Waiting Data</h1></Col>
            </Row>
          </Container>
        </div>
      )
    );
  }
}

export default Home;
