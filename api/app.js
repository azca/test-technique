// Dependencies
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const validator = require('express-validator');
const helmet = require('helmet');

const express_graphql = require('express-graphql');
const { buildSchema } = require('graphql');

// Express initialisation
const app = express();

// ------------------------------------------------ Middlewares
app.use(bodyParser.json({
	limit: '500mb',
}));

app.use(bodyParser.urlencoded({
	extended: true,
	limit: '500mb',
	parameterLimit: 1000000,
}));

app.use(validator());

// ---------------------------------------- API Protection with Helmet
app.use(helmet());

// ----------------------------- Allow cross-origin resource sharing
app.use(cors());
app.options(cors());

// ----------------------------------------- Log les requêtes (development)
app.use(morgan('dev'));


// GraphQL schema
const schema = buildSchema(`
		type Query {
			colors(idColor: ID, name: String, hexa: String): [Color]
			products(idProduct: ID, name: String, color: ID): [Product]
		}
    type Color {
			idColor: ID
			name: String
			hexa: String
		}
		type Product {
			idProduct: ID
			name: String
			color: Color
		}
`);

const colors = [

	{ idColor: 1, name: 'green', hexa: '#00ff00' },
 
	{ idColor: 2, name: 'red', hexa: '#ff0000' },
 
	{ idColor: 3, name: 'blue', hexa: '#0000ff' },
 ];


 const products = [

	{ idProduct: 1, name: 'T-Shirt', idColor: 1 },
 
	{ idProduct: 2, name: 'Sweat', idColor: 1 },
 
	{ idProduct: 3, name: 'Polo', idColor: 1 },
 ];

const getColors = function(args) {
    if (args.name) {
        const name = args.name;
        return colors.filter(color => color.name === name);
    } else {
        return colors;
    }
}

const getProducts = function(args) {

	if (args.name) {
			const name = args.name;
			return products.filter(product => product.name === name);
	} else {
			return products;
	}
}
const root = {
		colors: getColors,
		products: getProducts
};

app.use('/graphql', express_graphql({
    schema: schema,
    rootValue: root,
    graphiql: true
}));



// -------------------------------------------------- Start Server
const PORT = 4000;

app.listen(PORT, () => {
    console.log(`API is running on port ${PORT} ...`);
});